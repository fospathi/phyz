package celestial_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
)

func TestGregorian_DayAfter(t *testing.T) {
	want := celestial.Gregorian{Y: 2018, M: 9, D: 13}
	got := celestial.Gregorian{Y: 2018, M: 9, D: 12}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2018, M: 10, D: 1}
	got = celestial.Gregorian{Y: 2018, M: 9, D: 30}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2019, M: 1, D: 1}
	got = celestial.Gregorian{Y: 2018, M: 12, D: 31}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2019, M: 3, D: 1}
	got = celestial.Gregorian{Y: 2019, M: 2, D: 28}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2020, M: 3, D: 1}
	got = celestial.Gregorian{Y: 2020, M: 2, D: 29}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2020, M: 2, D: 29}
	got = celestial.Gregorian{Y: 2020, M: 2, D: 28}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestGregorian_JDN(t *testing.T) {
	want := celestial.JDN(0)
	got := celestial.Gregorian{Y: -4713, M: 11, D: 24}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(1721120)
	got = celestial.Gregorian{Y: 0, M: 3, D: 1}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(2451545)
	got = celestial.Gregorian{Y: 2000, M: 1, D: 1}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(2458374)
	got = celestial.Gregorian{Y: 2018, M: 9, D: 12}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g := celestial.Gregorian{Y: -4713, M: 11, D: 22}
	for jdn := -2; jdn < 2458374; jdn++ {
		want = celestial.JDN(jdn)
		got = g.JDN()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			break
		}
		g = g.DayAfter()
	}

	{
		want := celestial.Gregorian{Y: 2018, M: 9, D: 12}
		got := g
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
