package celestial

// The number of days in eleven months of the year, excluding February, starting
// with March: March, April, ..., December, January
var marchCal11 = [12]int{31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31}

// The number of days in the months of a leap year starting with March: March,
// April, ..., January, February.
var marchCalLeap = [12]int{31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31, 29}

// The number of days in the months of a non-leap year starting with March:
// March, April, ..., January, February.
var marchCalNonLeap = [12]int{31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31, 28}

// The number of days in the months of a leap year starting with January:
// January, February, ..., December.
var janCalLeap = [12]int{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

// The number of days in the months of a non-leap year starting with January:
// January, February, ..., December.
var janCalNonLeap = [12]int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}

// DaysSinceMarch1st returns, for the given month and day, the number of days
// since the previous March 1st.
func DaysSinceMarch1st(m, d int) int {
	if m < 3 {
		m += 12
	}
	days := 0
	for i, l := 0, len(marchCal11); i < l; i++ {
		if m == i+3 {
			break
		}
		days += marchCal11[i]
	}
	days += (d - 1)
	return days
}
