package celestial

import "math"

// JulianMeanYear is the average number of calendar days in a Julian calendar
// year in the repeating 28 year Julian period.
//
// The total number of calendar days in a Julian repeating period is (28*365) +
// (28/4) = 10227. Now divide by the number of calendar years in that period
// (28) to get 365.25.
const JulianMeanYear = 365.25

// JulianRep is the smallest number of calendar days that may be added to any
// date in the Julian calendar such that the day of the week, day, and month of
// the resulting date will be the same as the original.
//
// JulianRep is the smallest natural number exactly divisible by seven which can
// be obtained by multiplying the Julian mean year by any natural number. See
// https://play.golang.org/p/J6nsOrY_o2W.
const JulianRep = 10227

// A JulianYear is a particular year in the Julian calendar specified in
// astronomical year numbering:
// https://en.wikipedia.org/wiki/Astronomical_year_numbering. In astronomical
// year numbering there is a year 0 and negative years are allowed.
type JulianYear int

// IsLeap shall return true if the receiver year is a leap year in the proleptic
// Julian calendar.
func (jy JulianYear) IsLeap() bool {
	return jy%4 == 0
}

// A Julian is a specific calendar day in the proleptic Julian calendar.
type Julian struct {
	/// The day, in the range 1..31.
	D int
	// The month, in the range 1..12.
	M int
	// The year.
	Y JulianYear
}

// DayAfter returns the next day after the receiver day.
func (j Julian) DayAfter() Julian {
	var monthDays [12]int
	if j.Y.IsLeap() {
		monthDays = janCalLeap
	} else {
		monthDays = janCalNonLeap
	}
	if j.D == monthDays[j.M-1] {
		if j.M == 12 {
			return Julian{Y: j.Y + 1, M: 1, D: 1}
		}
		return Julian{Y: j.Y, M: j.M + 1, D: 1}
	}
	return Julian{Y: j.Y, M: j.M, D: j.D + 1}
}

// JDN returns the Julian Day Number of the solar day which starts at noon on
// the receiver calendar day.
func (j Julian) JDN() JDN {
	const jdn0 int = 1721118 // Julian(0, 3, 1) <==> Julian Day Number 1721120
	const yPeriod int = 28
	const dPeriod int = yPeriod*365 + 28/4
	var yShift, dShift int
	if j.Y < 1 {
		yShift = int(math.Ceil(float64(1-j.Y)/float64(yPeriod))) * yPeriod
		dShift = (yShift / yPeriod) * dPeriod
	}
	y, m, d := int(j.Y)+yShift, j.M, j.D
	if m < 3 {
		m += 12
		y--
	}
	d0 := DaysSinceMarch1st(m, d) + y*365 + y/4
	return JDN(jdn0 + d0 - dShift)
}
