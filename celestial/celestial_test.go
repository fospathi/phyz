package celestial_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
)

func TestDaysSinceMarch1st(t *testing.T) {
	want := 0
	got := celestial.DaysSinceMarch1st(3, 1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 24
	got = celestial.DaysSinceMarch1st(3, 25)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 194
	got = celestial.DaysSinceMarch1st(9, 11)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 336
	got = celestial.DaysSinceMarch1st(1, 31)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 364
	got = celestial.DaysSinceMarch1st(2, 28)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
