package celestial_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
)

func TestJulian_DayAfter(t *testing.T) {
	want := celestial.Julian{Y: 2018, M: 9, D: 13}
	got := celestial.Julian{Y: 2018, M: 9, D: 12}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2018, M: 10, D: 1}
	got = celestial.Julian{Y: 2018, M: 9, D: 30}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2019, M: 1, D: 1}
	got = celestial.Julian{Y: 2018, M: 12, D: 31}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2019, M: 3, D: 1}
	got = celestial.Julian{Y: 2019, M: 2, D: 28}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2020, M: 3, D: 1}
	got = celestial.Julian{Y: 2020, M: 2, D: 29}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2020, M: 2, D: 29}
	got = celestial.Julian{Y: 2020, M: 2, D: 28}.DayAfter()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestJulian_JDN(t *testing.T) {
	want := celestial.JDN(0)
	got := celestial.Julian{Y: -4712, M: 1, D: 1}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(1721118)
	got = celestial.Julian{Y: 0, M: 3, D: 1}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(2451558)
	got = celestial.Julian{Y: 2000, M: 1, D: 1}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.JDN(2458387)
	got = celestial.Julian{Y: 2018, M: 9, D: 12}.JDN()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	j := celestial.Julian{Y: -4713, M: 12, D: 30}
	for jdn := -2; jdn < 2458387; jdn++ {
		want = celestial.JDN(jdn)
		got = j.JDN()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			break
		}
		j = j.DayAfter()
	}

	{
		want := celestial.Julian{Y: 2018, M: 9, D: 12}
		got := j
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
