package celestial_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
)

func TestJDN_Gregorian(t *testing.T) {
	want := celestial.Gregorian{Y: -4713, M: 11, D: 24}
	got := celestial.JDN(0).Gregorian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 0, M: 3, D: 1}
	got = celestial.JDN(1721120).Gregorian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2000, M: 1, D: 1}
	got = celestial.JDN(2451545).Gregorian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Gregorian{Y: 2018, M: 9, D: 12}
	got = celestial.JDN(2458374).Gregorian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	g := celestial.Gregorian{Y: -4713, M: 11, D: 22}
	for jdn := -2; jdn < 2458374; jdn++ {
		want = g
		got = celestial.JDN(jdn).Gregorian()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			break
		}
		g = g.DayAfter()
	}

	want = celestial.Gregorian{Y: 2018, M: 9, D: 12}
	got = g
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestJDN_Julian(t *testing.T) {
	want := celestial.Julian{Y: -4712, M: 1, D: 1}
	got := celestial.JDN(0).Julian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 0, M: 3, D: 1}
	got = celestial.JDN(1721118).Julian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2000, M: 1, D: 1}
	got = celestial.JDN(2451558).Julian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = celestial.Julian{Y: 2018, M: 9, D: 12}
	got = celestial.JDN(2458387).Julian()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	j := celestial.Julian{Y: -4713, M: 12, D: 30}
	for jdn := -2; jdn < 2458387; jdn++ {
		want = j
		got = celestial.JDN(jdn).Julian()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			break
		}
		j = j.DayAfter()
	}
}

func TestYMD(t *testing.T) {
	y, m, d := celestial.YMD(0, false)
	want := true
	got := y == 0 && m == 3 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(1, false)
	want = true
	got = y == 0 && m == 3 && d == 2
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(30, true)
	want = true
	got = y == 0 && m == 3 && d == 31
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(31, true)
	want = true
	got = y == 0 && m == 4 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(60, false)
	want = true
	got = y == 0 && m == 4 && d == 30
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(61, false)
	want = true
	got = y == 0 && m == 5 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Ma   Ap   Ma   Ju   Ju   Au   Se   Oc   No   De   Ja   Fe
	// 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31 + 31 + 28
	//      61   92   122  153  184  214  245  275  306  337  365

	y, m, d = celestial.YMD(61, false)
	want = true
	got = y == 0 && m == 5 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(184, false)
	want = true
	got = y == 0 && m == 9 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(305, false)
	want = true
	got = y == 0 && m == 12 && d == 31
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(306, false)
	want = true
	got = y == 1 && m == 1 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(336, true)
	want = true
	got = y == 1 && m == 1 && d == 31
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(365, false)
	want = true
	got = y == 1 && m == 3 && d == 1
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(365, true)
	want = true
	got = y == 1 && m == 2 && d == 29
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	y, m, d = celestial.YMD(367, true)
	want = true
	got = y == 1 && m == 3 && d == 2
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
