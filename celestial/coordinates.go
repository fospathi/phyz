package celestial

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Degree is a unit of angle measurement, 360 degrees per turn.
type Degree float64

// Radian is a unit of angle measurement, 2Pi radians per turn.
type Radian float64

// Cos returns the cos of the receiver angle.
func (d Degree) Cos() float64 {
	return math.Cos((float64(d) / 360) * maf.TwoPi)
}

// Positive returns the principle trigonometric plane equivalent to the receiver
// angle in the range 0..360.
func (d Degree) Positive() Degree {
	r := d.Rads()
	r = r.ModuloPi()
	if r < 0 {
		r += maf.TwoPi
	}
	return r.Degs()
}

// Rads expresses the receiver angle in radians.
func (d Degree) Rads() Radian {
	return Radian((d / 360) * maf.TwoPi)
}

// Sin returns the sin of the receiver angle.
func (d Degree) Sin() float64 {
	return math.Sin((float64(d) / 360) * maf.TwoPi)
}

// Degs expresses the receiver angle in degrees.
func (r Radian) Degs() Degree {
	return Degree((r / maf.TwoPi) * 360)
}

// ModuloPi returns the principle trigonometric plane equivalent to the receiver
// angle in the range -PI..PI.
func (r Radian) ModuloPi() Radian {
	if r < -math.Pi || r > math.Pi {
		return Radian(math.Atan2(math.Sin(float64(r)), math.Cos(float64(r))))
	}
	return r
}

// AstronomicalAngle is an angle in degrees.
type AstronomicalAngle float64

// HMSHours is the hours component of the receiver angle when expressed in the
// Hours, Minutes, and Seconds format.
//
// The returned value is a positive integer in the range 0..24.
func (aa AstronomicalAngle) HMSHours() int {
	pos := float64(Degree(aa).Positive())
	return int((24.0/360.0)*pos) % 24
}

// DMSDegrees is the degrees component of the receiver angle when expressed in
// the Degrees, Minutes, and Seconds format.
func (aa AstronomicalAngle) DMSDegrees() int {
	return int(aa) % 360
}

// EquatorialCoordinates is a point in a equatorial coordinate system:
// https://en.wikipedia.org/wiki/Equatorial_coordinate_system.
type EquatorialCoordinates struct {
	// The coordinate which uses the direction of the vernal equinox as its
	// positive axis.
	X float64
	// The coordinate whose positive axis is the vector cross product of the
	// Z-axis with the X-axis, in that order.
	Y float64
	// The coordinate which uses the direction of the celestial north pole as
	// its positive axis.
	Z float64
}

// Declination is the signed angle, measured in degrees, between the celestial
// equator's plane and the line segment joining the receiver position to the
// origin.
//
// The north celestial pole has a [declination] of +90 degrees.
//
// [declination]: https://en.wikipedia.org/wiki/Declination
func (ec EquatorialCoordinates) Declination() Degree {
	result := Degree(0)
	if ec.Z != 0 {
		result = Degree(maf.RadToDeg(
			math.Atan(ec.Z / math.Sqrt(ec.X*ec.X+ec.Y*ec.Y)),
		))
	}
	return result
}

// RightAscension ([right ascension]) is the signed angle, measured in degrees,
// between the equinox and the orthogonal projection onto the celestial equator
// of the line segment joining the receiver position to the origin.
//
// [right ascension]: https://en.wikipedia.org/wiki/Right_ascension
func (ec EquatorialCoordinates) RightAscension() Degree {
	return Degree(maf.RadToDeg(math.Atan2(ec.Y, ec.X)))
}

// SphericalCoordinates is a point in a spherical coordinate system:
// https://en.wikipedia.org/wiki/Spherical_coordinate_system
type SphericalCoordinates struct {
	Radius  float64
	Polar   Degree
	Azimuth Degree
}

// Equatorial returns the receiver position in equatorial coordinates.
func (sc SphericalCoordinates) Equatorial() EquatorialCoordinates {
	base := sc.Radius * sc.Polar.Sin()
	return EquatorialCoordinates{
		X: base * sc.Azimuth.Cos(),
		Y: base * sc.Azimuth.Sin(),
		Z: sc.Radius * sc.Polar.Cos(),
	}
}
