package celestial

import (
	"math"
)

// GregorianMeanYear is the average number of calendar days in a Gregorian
// calendar year in the repeating 400 year Gregorian period.
//
// The total number of calendar days in a Gregorian repeating period is
// (400*365) + (400/4) - (400/100) + (400/400) = 146097. Now divide by the
// number of calendar years in that period (400) to get 365.2425.
const GregorianMeanYear = 365.2425

// GregorianRep is the smallest number of calendar days that may be added to any
// date in the Gregorian calendar such that the day of the week, day, and month
// of the resulting date will be the same as the original.
//
// GregorianRep is the smallest natural number which can be obtained by
// multiplying the Gregorian mean year by any natural number, and it is also
// exactly divisible by 7. See https://play.golang.org/p/A93sPYHxEO9.
const GregorianRep = 146097

// A GregorianYear is a particular year in the Gregorian calendar specified in
// astronomical year numbering:
// https://en.wikipedia.org/wiki/Astronomical_year_numbering. In astronomical
// year numbering there is a year 0 and negative years are allowed.
type GregorianYear int

// IsLeap shall return true if the receiver year is a leap year in the proleptic
// Gregorian calendar.
func (gy GregorianYear) IsLeap() bool {
	return (gy%400 == 0) || ((gy%4 == 0) && (gy%100 != 0))
}

// A Gregorian is a specific calendar day in the proleptic Gregorian calendar.
type Gregorian struct {
	/// The day, in the range 1..31.
	D int
	// The month, in the range 1..12.
	M int
	// The year.
	Y GregorianYear
}

// DayAfter returns the next day after the receiver day.
func (g Gregorian) DayAfter() Gregorian {
	var monthDays [12]int
	if g.Y.IsLeap() {
		monthDays = janCalLeap
	} else {
		monthDays = janCalNonLeap
	}
	if g.D == monthDays[g.M-1] {
		if g.M == 12 {
			return Gregorian{Y: g.Y + 1, M: 1, D: 1}
		}
		return Gregorian{Y: g.Y, M: g.M + 1, D: 1}
	}
	return Gregorian{Y: g.Y, M: g.M, D: g.D + 1}
}

// JD returns the Julian Date at noon on the receiver calendar day.
func (g Gregorian) JD() JD {
	return JD{JDN: g.JDN(), Fraction: 0}
}

// JDN returns the Julian Day Number of the solar day which starts at noon on
// the receiver calendar day.
func (g Gregorian) JDN() JDN {
	const jdn0 int = 1721120 // Gregorian(0, 3, 1) <==> Julian Day Number 1721120
	const yPeriod int = 400
	const dPeriod int = yPeriod*365 + 400/(400) - 400/100 + 400/4
	var yShift, dShift int
	if g.Y < 1 {
		yShift = int(math.Ceil(float64(1-g.Y)/float64(yPeriod))) * yPeriod
		dShift = (yShift / yPeriod) * dPeriod
	}
	y, m, d := int(g.Y)+yShift, g.M, g.D
	if m < 3 {
		m += 12
		y--
	}
	d0 := DaysSinceMarch1st(m, d) + y*365 + y/400 - y/100 + y/4
	return JDN(jdn0 + d0 - dShift)
}
