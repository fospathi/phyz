# Package celestial

```go
import "gitlab.com/fospathi/phyz/celestial"
```

Go package celestial contains utilities for finding the positions of celestial bodies.
