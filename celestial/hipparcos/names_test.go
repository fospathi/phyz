package hipparcos_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial/hipparcos"
)

func TestCommonStarNames(t *testing.T) {

	vco, err := hipparcos.Extract(hipparcos.Records)

	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}
	{
		want := hipparcos.Records
		got := len(vco)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Make sure we are not including any common star names that don't have an
	// entry in the CSV data.

	for hip, name := range hipparcos.CommonStarNames {
		if !hipparcos.Contains(vco, hip) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", name, false)
		}
	}
}
