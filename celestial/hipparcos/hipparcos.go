/*
Package hipparcos reproduces a subset of the Hipparcos star catalog.
*/
package hipparcos

import (
	"bytes"
	_ "embed"
	"encoding/csv"
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/wire/colour"
)

// A CelestialObject from the Hipparcos star catalog.
type CelestialObject struct {
	ApparentMagnitude float64
	BVColorIndex      float64 // The B-V colour/temperature of the object.
	Declination       float64 // Declination relative to the Equatorial frame measured in units of degrees.
	HIP               int     // Hipparcos identifier: a unique ID number in the Hipparcos star catalog for this celestial object.
	Parallax          float64 // Parallax measured in units of milliarcsecond (mas).
	RightAscension    float64 // Right ascension relative to the Equatorial frame measured in units of degrees.

	// A small number of the most notable celestial objects are given a common
	// name that they are known by. For most objects this is empty.
	//
	// Note that a common name if present is not reproduced from the Hipparcos
	// catalog.
	CommonName string
}

func (co CelestialObject) Colour() colour.RGB {
	// https://en.wikipedia.org/wiki/Color_index
	//
	// Assume the object is a black body and infer its temperature.
	bv := 0.92 * co.BVColorIndex
	temp := 4600 * ((1 / (bv + 1.7)) + (1 / (bv + 0.62)))
	temp = interval.In{1667, 25000}.Clamp(temp)

	// https://en.wikipedia.org/wiki/Planckian_locus#Approximation
	//
	// Convert the temperature to 1931 CIE (x, y) chromaticity coordinates. For
	// a black body the coordinates are somewhere along the Planckian locus.
	var x, y float64
	switch {
	case temp <= 4000:
		x = -0.2661239*(1e9/(temp*temp*temp)) +
			-0.2343589*(1e6/(temp*temp)) +
			0.8776956*(1e3/(temp)) +
			0.179910
	default:
		x = -3.0258469*(1e9/(temp*temp*temp)) +
			-2.1070379*(1e6/(temp*temp)) +
			0.2226347*(1e3/(temp)) +
			0.240390
	}
	x2, x3 := x*x, x*x*x
	switch {
	case temp <= 2222:
		y = -1.1063814*x3 - 1.34811020*x2 + 2.18555832*x - 0.20219683
	case temp <= 4000:
		y = -0.9549476*x3 - 1.37418593*x2 + 2.09137015*x - 0.16748867
	default:
		y = +3.0817580*x3 - 5.87338670*x2 + 3.75112997*x - 0.37001483
	}

	// https://en.wikipedia.org/wiki/CIE_1931_color_space#Meaning_of_X,_Y_and_Z
	//
	// Get the 1931 CIE (X, Y, Z) coordinates. Use a luminance of 1.
	X := x / y
	Y := 1.0
	Z := (1 / y) * (1 - x - y)

	// https://www.w3.org/TR/css-color-4/#color-conversion-code
	//
	// Obtain the linear RGB equivalent colour from the XYZ coordinates.
	lRGB := maf.Mat3{
		3.2409699419045226, -1.537383177570094, -0.4986107602930034,
		-0.9692436362808796, 1.8759675015077202, 0.04155505740717559,
		0.05563007969699366, -0.20397695888897652, 1.0569715142428786,
	}.Transform(maf.Vec{X: X, Y: Y, Z: Z})

	// Convert linear to sRGB.
	toGamma := func(val float64) float64 {
		if val > 0.0031308 {
			return 1.055*math.Pow(val, 1/2.4) - 0.055
		}
		return 12.92 * val
	}
	rgb := lRGB.Map(toGamma)

	return colour.NewRGB(rgb)
}

// Distance to the celestial object measured in parsecs (pc).
func (co CelestialObject) Distance() float64 {
	// http://spiff.rit.edu/classes/phys440/lectures/helio_para/helio_para.html

	plx := maf.DegToRad(co.Parallax / (1000.0 * 60.0 * 60.0))
	l := 1 / math.Tan(plx)        // Distance in astronomical units (au).
	return l / (648000 / math.Pi) // Convert au to pc.
}

// Ecliptic coordinates for the intersection point of:
//
// 1. the half-line from the origin of the J2000 Ecliptic frame to the celestial
// object;
//
// 2. the unit sphere with center at the origin of the J2000 Ecliptic frame.
//
// So essentially this is a unit vector pointing from the frame origin to the
// object position.
func (co CelestialObject) Ecliptic() maf.Vec {
	var (
		// A standard frame initially coinciding with the equatorial frame.
		//
		// Let the starting point coincide with the positive X-axis unit vector.
		b = maf.StdBasis
		// Right ascension is a rotation around the equatorial north pole, which
		// is the positive Z-axis.
		ra = maf.NewZRot(maf.DegToRad(co.RightAscension))
		// Assuming the whole frame was rotated around the Z-axis by the right
		// ascension, declination is then a rotation around the local Y-axis,
		// which moves the X-axis to coincide with the target point.
		dec = maf.NewYRot(maf.DegToRad(-co.Declination))
	)

	return EquatorialToEcliptic.Transform(b.Local(ra).Local(dec).X).Unit()
}

// Extract the first n celestial objects from the comma-separated values file
// hipparcos.csv.
//
// The celestial objects are already sorted by apparent brightness with the
// brightest first.
func Extract(n int) ([]CelestialObject, error) {
	if n < 0 || n > Records {
		return nil,
			fmt.Errorf("range error: expected 0 <= n <= %v: got %v", Records, n)
	}

	var (
		r   = csv.NewReader(bytes.NewReader(csvData))
		vco []CelestialObject
	)
	r.FieldsPerRecord = fieldsPerRecord

	for i := 0; i < n+1; i++ {
		record, err := r.Read()
		if err != nil {
			return nil, err
		}
		if i == 0 {
			if strings.Join(record, ",") != headings {
				return nil,
					errors.New("incorrect column name(s): expected " + headings)
			}
			continue
		}
		var (
			hip    int
			vMag   float64
			bv     float64
			plxNew float64
			de     float64
			ra     float64
		)
		for j := 0; j < fieldsPerRecord; j++ {
			switch j {
			case 0:
				hip, err = strconv.Atoi(record[j])
			case 1:
				vMag, err = strconv.ParseFloat(record[j], 64)
			case 2:
				bv, err = strconv.ParseFloat(record[j], 64)
			case 3:
				// Skip the old parallax field in favour of the new improved one.
			case 4:
				plxNew, err = strconv.ParseFloat(record[j], 64)
			case 5:
				de, err = strconv.ParseFloat(record[j], 64)
			case 6:
				ra, err = strconv.ParseFloat(record[j], 64)
			}
			if err != nil {
				return nil, err
			}
		}
		vco = append(vco, CelestialObject{
			ApparentMagnitude: vMag,
			BVColorIndex:      bv,
			Declination:       de,
			HIP:               hip,
			Parallax:          plxNew,
			RightAscension:    ra,

			CommonName: CommonStarNames[hip],
		})
	}

	return vco, nil
}

var (
	obliquity = float64(solsys.EclipticObliquity.Rads())

	// EquatorialToEcliptic is a change of frame transformation from the J2000
	// ICRF/equatorial frame to the J2000 Ecliptic place.
	//
	// The J1991.25 Hipparcos reference frame almost coincides with the J2000
	// ICRF/equatorial frame.
	EquatorialToEcliptic = maf.StdBasis.Local(maf.NewXRot(obliquity)).In()
)

const (
	// The maximum number of celestial objects which can be extracted from the
	// hipparcos.csv data.
	Records = 50000

	fieldsPerRecord = 7
	headings        = "hip,vmag,b_v,plx,plx_new,de,ra"
)

//go:embed hipparcos.csv
var csvData []byte
