package hipparcos_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial/hipparcos"
	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/universal/interval"
)

func TestCelestialObject_Distance(t *testing.T) {

	// Test the distances of the three brightest celestial objects.

	n := 3
	vco, err := hipparcos.Extract(n)

	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}
	{
		want := n
		got := len(vco)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	var (
		sirius   = vco[0]
		canopus  = vco[1]
		arcturus = vco[2]
	)

	{
		siriusTolerance := 0.05
		siriusDistance := 2.64
		want := true
		got := interval.In{
			siriusDistance - siriusTolerance,
			siriusDistance + siriusTolerance,
		}.AsClosedContains(sirius.Distance())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		canopusTolerance := 3.0
		canopusDistance := 95.0
		want := true
		got := interval.In{
			canopusDistance - canopusTolerance,
			canopusDistance + canopusTolerance,
		}.AsClosedContains(canopus.Distance())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		arcturusTolerance := 0.1
		arcturusDistance := 11.26
		want := true
		got := interval.In{
			arcturusDistance - arcturusTolerance,
			arcturusDistance + arcturusTolerance,
		}.AsClosedContains(arcturus.Distance())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestCelestialObject_Ecliptic(t *testing.T) {

	vco, err := hipparcos.Extract(hipparcos.Records)

	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}
	{
		want := hipparcos.Records
		got := len(vco)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the Ecliptic reference frame coordinates of the unit vector pointing
	// from the origin to 29 Piscium.
	//
	// 29 Piscium is pretty close to the vernal equinox so the angle between the
	// unit vector and the positive X-axis should be quite small.

	var piscium hipparcos.CelestialObject
	for _, co := range vco {
		if co.HIP == 145 {
			piscium = co
		}
	}
	{
		want := true
		got := piscium.HIP == 145
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	pisciumTolerance := maf.DegToRad(5)
	{
		want := true
		got := maf.PosX.Angle(piscium.Ecliptic()) < pisciumTolerance
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the Ecliptic reference frame coordinates of the unit vector pointing
	// from the origin to Polaris.
	//
	// Polaris is pretty close to the Earth's axis of rotation so the angle
	// between the unit vector and the positive Z-axis should be about the same
	// as the Earth's axial tilt.

	var polaris hipparcos.CelestialObject
	for _, co := range vco {
		if co.HIP == 11767 {
			polaris = co
		}
	}
	{
		want := true
		got := polaris.HIP == 11767
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	var (
		angToZ           = maf.RadToDeg(maf.PosZ.Angle(polaris.Ecliptic()))
		obliquity        = float64(solsys.EclipticObliquity)
		polarisTolerance = 1.0
		in               = interval.In{
			obliquity - polarisTolerance,
			obliquity + polarisTolerance,
		}
	)
	{
		want := true
		got := in.AsClosedContains(angToZ)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the Ecliptic reference frame coordinates of the unit vector pointing
	// from the origin to the Southern Ecliptic Pole.
	//
	// The angle between the unit vector and the negative Z-axis should be quite
	// small.
	//
	// The Southern Ecliptic Pole passes through the apparent midpoint of the
	// stars Delta Doradus and Eta1 Doradus. Thus the unit vector to be tested
	// can be formed from the average of the unit vectors pointing to these two
	// stars.

	var deltaDoradus, eta1Doradus hipparcos.CelestialObject
	for _, co := range vco {
		switch co.HIP {
		case 27100:
			deltaDoradus = co
		case 28909:
			eta1Doradus = co
		}
	}
	{
		want := true
		got := deltaDoradus.HIP == 27100 && eta1Doradus.HIP == 28909
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	var (
		doradoTolerance = maf.DegToRad(2)
		p1, p2          = deltaDoradus.Ecliptic(), eta1Doradus.Ecliptic()
		midPoint        = p1.MidTo(p2).Unit()
	)
	{
		want := true
		got := maf.NegZ.Angle(midPoint) < doradoTolerance
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test the Ecliptic reference frame coordinates of the unit vector pointing
	// from the origin to Sirius.
	//
	// The unit vector pointing to Sirius should have a slightly negative
	// X-coordinate, a significantly positive Y-coordinate and a negative
	// Z-coordinate.

	sirius := vco[0].Ecliptic()
	{
		want := true
		got := sirius.X < 0 && sirius.Y > 0.5 && sirius.Z < 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestExtract(t *testing.T) {

	// Test extracting too many records.

	_, err := hipparcos.Extract(hipparcos.Records + 1)
	if err == nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "range error", nil)
	}

	// Test extracting the first and visually brightest celestial object.

	vco, err := hipparcos.Extract(1)
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}
	{
		want := 1
		got := len(vco)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	sirius := vco[0]
	{
		want := true
		got := sirius.HIP == 32349 &&
			sirius.ApparentMagnitude == -1.44 &&
			sirius.BVColorIndex == 0.009 &&
			sirius.Parallax == 379.21 &&
			sirius.Declination == -16.71314306 &&
			sirius.RightAscension == 101.28854105
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test extracting all records.

	vco, err = hipparcos.Extract(hipparcos.Records)
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}
	{
		want := hipparcos.Records
		got := len(vco)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	HIP53715 := vco[hipparcos.Records-1]
	{
		want := true
		got := HIP53715.HIP == 53715 &&
			HIP53715.ApparentMagnitude == 8.23 &&
			HIP53715.BVColorIndex == 1.158 &&
			HIP53715.Parallax == 4.05 &&
			HIP53715.Declination == -21.04684077 &&
			HIP53715.RightAscension == 164.85831204
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
