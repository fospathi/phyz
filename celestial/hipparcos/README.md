# Package hipparcos

```go
import "gitlab.com/fospathi/phyz/celestial/hipparcos"
```

Go package hipparcos provides positions, magnitudes, and colours for 50,000 particularly bright celestial objects located outside the Solar System. 

The data for the 50,000 objects is reproduced from the [Hipparcos](https://en.wikipedia.org/wiki/Hipparcos) catalog. Note that the 50,000 objects included here are only a subset of the full Hipparcos catalog which contains over 100,000 celestial objects.

It is assumed by this package that the Hipparcos reference frame (HCRF) coincides with the [ICRF](https://en.wikipedia.org/wiki/International_Celestial_Reference_System_and_its_realizations). The Hipparcos catalog uses the J1991.25 epoch but this package doesn't include any data describing time dependant changes to the celestial objects so the epoch is not particularly relevant.

## Obtaining the data

The CSV data reproduced here in the *hipparcos.csv* file can also be obtained from the ESA's [archive](https://gea.esac.esa.int/archive/).

To generate the CSV data first go to Search and then go to the Advanced(ADQL) tab. The ADQL query used to generate the CSV data is:

    SELECT TOP 50000 h.hip, h.vmag, h.b_v, h.plx, hnew.plx as plx_new, h.de, h.ra
    FROM public.hipparcos AS h
    LEFT OUTER JOIN public.hipparcos_newreduction AS hnew USING (hip)
    WHERE h.plx IS NOT NULL
    AND h.b_v IS NOT NULL
    ORDER BY vmag ASC, hip ASC

After the query completes select CSV from the "Download format" drop-down menu before downloading the data.

## Meaning of the data

As you can see from the ADQL query the CSV data is mostly reproduced from the archive's "public.hipparcos" catalog but the *plx_new* column in the CSV data is the *plx* column from the archive's "public.hipparcos_newreduction" catalog.

The column headings of the CSV data are:
  - hip: Hipparcos identifier
  - vmag: apparent magnitude
  - b_v: B-V colour index
  - plx: parallax
  - plx_new: new improved parallax
  - de: declination (degrees)
  - ra: right ascension (degrees)