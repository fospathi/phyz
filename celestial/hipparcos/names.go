package hipparcos

// Contains is true if the given slice of celestial objects contains an object
// with the given Hipparcos identifier (HIP).
func Contains(vco []CelestialObject, hip int) bool {
	for _, co := range vco {
		if co.HIP == hip {
			return true
		}
	}
	return false
}

// CommonStarNames keyed by their Hipparcos identifier (HIP).
//
// The star names are reproduced from
// https://www.cosmos.esa.int/web/hipparcos/common-star-names and
// https://www.cosmos.esa.int/web/hipparcos/brightest. Some star names are not
// included however as they don't have a counterpart in this package which is
// only a subset of the full Hipparcos catalog.
var CommonStarNames = map[int]string{
	13847:  "Acamar",
	7588:   "Achernar",
	60718:  "Acrux",
	33579:  "Adhara",
	68702:  "Agena",
	95947:  "Albireo",
	65477:  "Alcor",
	17702:  "Alcyone",
	21421:  "Aldebaran",
	105199: "Alderamin",
	1067:   "Algenib",
	50583:  "Algieba",
	14576:  "Algol",
	31681:  "Alhena",
	62956:  "Alioth",
	67301:  "Alkaid",
	9640:   "Almaak",
	109268: "Alnair",
	25428:  "Alnath",
	26311:  "Alnilam",
	26727:  "Alnitak",
	46390:  "Alphard",
	76267:  "Alphekka",
	677:    "Alpheratz",
	98036:  "Alshain",
	97649:  "Altair",
	2081:   "Ankaa",
	80763:  "Antares",
	69673:  "Arcturus",
	25985:  "Arneb",
	82273:  "Atria",
	41037:  "Avior",
	25336:  "Bellatrix",
	27989:  "Betelgeuse",
	30438:  "Canopus",
	24608:  "Capella",
	746:    "Caph",
	36850:  "Castor",
	63125:  "Cor Caroli",
	102098: "Deneb",
	57632:  "Denebola",
	3419:   "Diphda",
	54061:  "Dubhe",
	107315: "Enif",
	87833:  "Etamin",
	113368: "Fomalhaut",
	61084:  "Gacrux",
	57939:  "Groombridge 1830",
	9884:   "Hamal",
	72105:  "Izar",
	90185:  "Kaus Australis",
	72607:  "Kocab",
	113963: "Markab",
	59774:  "Megrez",
	28360:  "Menkalinan",
	14135:  "Menkar",
	53910:  "Merak",
	45238:  "Miaplacidus",
	25930:  "Mintaka",
	10826:  "Mira",
	5447:   "Mirach",
	15863:  "Mirphak",
	30324:  "Mirzam",
	65378:  "Mizar",
	25606:  "Nihal",
	92855:  "Nunki",
	100751: "Peacock",
	58001:  "Phad",
	17851:  "Pleione",
	11767:  "Polaris",
	37826:  "Pollux",
	37279:  "Procyon",
	84345:  "Rasalgethi",
	86032:  "Rasalhague",
	49669:  "Regulus",
	24436:  "Rigel",
	71683:  "Rigil Kent",
	109074: "Sadalmelik",
	27366:  "Saiph",
	113881: "Scheat",
	85927:  "Shaula",
	3179:   "Shedir",
	92420:  "Sheliak",
	32349:  "Sirius",
	65474:  "Spica",
	97278:  "Tarazed",
	68756:  "Thuban",
	77070:  "Unukalhai",
	91262:  "Vega",
	63608:  "Vindemiatrix",
	18543:  "Zaurak",
}
