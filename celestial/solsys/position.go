package solsys

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial"
)

// PlanetPosition and associated Keplerian elements of a planet with respect to
// the mean equinox and ecliptic of J2000.0.
type PlanetPosition struct {
	// The rectangular ecliptic coordinates of the planet.
	Ecliptic maf.Vec
	// The unit tangent vector to the planet's orbit in rectangular ecliptic
	// coordinates.
	EclipticTangent maf.Vec

	// The rectangular equatorial coordinates of the planet in the ICRF/J2000
	// frame.
	Equatorial celestial.EquatorialCoordinates
	// The rectangular heliocentric coordinates of the planet.
	Heliocentric maf.Vec

	ArgumentOfPerihelion        celestial.Radian
	EccentricAnomaly            celestial.Radian
	Eccentricity                float64
	Inclination                 celestial.Radian
	LongitudeOfTheAscendingNode celestial.Radian
	LongitudeOfPerihelion       celestial.Radian
	MeanAnomaly                 celestial.Radian
	MeanLongitude               celestial.Radian
	SemiMajorAxis               float64 // The semi-major axis measured in astronomical units.
}

// NewPlanetPosition returns the approximate position of the argument planet on
// the argument date which shall be a date within the argument time period.
//
// For the planet Earth it is the position of the Earth-Moon Barycenter which is
// calculated.
func NewPlanetPosition(
	planet Planet,
	jd celestial.JD,
	period Interval,
) PlanetPosition {

	var elements Elements
	if Y250 == period {
		elements = ElementsY250[planet]
	} else {
		elements = ElementsY6000[planet]
	}

	ml := elements.MeanLongitude.OnDate(jd)
	lp := elements.LongitudeOfPerihelion.OnDate(jd)

	// The perihelion is the point of closest approach between the planet and
	// the sun. The argument of perihelion is the angle atwixt perihelion and
	// the ascending node.
	argumentOfPerihelion := lp - elements.LongitudeOfTheAscendingNode.OnDate(jd)

	// The mean anomaly (M) is the angle from the perihelion which a fictitious
	// body would have if it moved in a circular orbit, with constant speed, in
	// the same orbital period as the actual body in its elliptical orbit.
	meanAnomaly := ml - lp + elements.Extra.MeanAnomalyExtra(planet, period, jd)
	M := celestial.Degree(meanAnomaly).Rads().ModuloPi()

	// The eccentric anomaly (E) is the parameter in the parameterisation of the
	// equation of the ellipse:
	//
	// {x = a cos(E), y = b sin(E)}
	//
	// where the X-axis is aligned in the direction from the focus/Sun to the
	// perihelion and the origin is at the centre of the elliptical orbit.
	//
	// Measured in radians.
	E := KeplerRoot(float64(M), elements.Eccentricity.OnDate(jd))

	a := elements.SemiMajorAxis.OnDate(jd)
	e := elements.Eccentricity.OnDate(jd)
	w := celestial.Degree(argumentOfPerihelion).Rads()
	I := celestial.Degree(elements.Inclination.OnDate(jd)).Rads()
	lan := celestial.Degree(elements.LongitudeOfTheAscendingNode.OnDate(jd)).Rads()

	// Once the eccentric anomaly is known, the heliocentric coordinates can be
	// calculated.
	heliocentric := maf.Vec{
		X: a * (math.Cos(E) - e),
		Y: a * math.Sqrt(1-e*e) * math.Sin(E),
		Z: 0,
	}

	heliocentricTangent := maf.Vec{ // Differentiate position wrt E then take its unit vector.
		X: -a * math.Sin(E),
		Y: a * math.Sqrt(1-e*e) * math.Cos(E),
		Z: 0,
	}.Unit()

	// Make the planet's heliocentric X-axis, that is the line joining the
	// origin at the Sun to the perihelion, coincide with the ecliptic plane.
	b1 := maf.StdBasis.Local(maf.NewZRot(float64(-w)))

	// Make the planet's orbital plane coincide with the ecliptic plane.
	b2 := maf.StdBasis.Local(maf.NewXRot(float64(-I)))

	// Finally, make the X-axes of both coordinate systems coincide.
	b3 := maf.StdBasis.Local(maf.NewZRot(float64(-lan)))

	// Remember, in matrix multiplication it is the rightmost transformation
	// that will be applied first.
	heliocentricToEcliptic := b3.In().Multiply(b2.In().Multiply(b1.In()))

	ecliptic := heliocentricToEcliptic.Transform(heliocentric)

	eclipticTangent :=
		heliocentricToEcliptic.Transform(heliocentricTangent).Unit()

	// A 3D diagram of the ecliptic coordinate system, on epoch J2000.0. The
	// little xs plot Earth's elliptical orbit in the ecliptic/XY plane. The X,
	// Y, and Z directions are mutually perpendicular. The Sun is at the origin
	// of the ecliptic's rectangular coordinate system.
	//
	//                     Z-direction
	//                          |
	//                          |
	//                          |   Earth on vernal equinox
	//                          |   O       x                    Earth's axial
	//                          |  /                 x         / tilt ~ 23.44°
	//                          | /                      x    /
	//                          |/                         x /
	//                     Sun  O - - - - - - -             /
	//                         /      Y-direction          O Earth on 2000/1/1
	//                        /                         x /
	//                       /                       x   /
	//                      /                    x      /
	//                     /                x          /
	//                    /  x    x
	//             X-direction
	//      vernal equinox direction - when Earth is at the vernal equinox draw
	//                                 a line from the Earth to the Sun
	//
	// Let Earth's equatorial coordinate system have the same X direction as the
	// ecliptic coordinate system, and its Z direction be its celestial north
	// pole. Then a negative rotation of the ecliptic coordinate system around
	// its own X-axis by the value of Earth's axial tilt (ecliptic obliquity)
	// will cause the ecliptic plane's axis directions to coincide with those of
	// Earth's celestial equator/equatorial coordinate system.
	b4 := maf.StdBasis.Local(maf.NewXRot(float64(-EclipticObliquity.Rads())))
	eclipticToEquatorial := b4.In()

	equatorial := eclipticToEquatorial.Transform(ecliptic)

	return PlanetPosition{
		Ecliptic:        ecliptic,
		EclipticTangent: eclipticTangent,
		Equatorial: celestial.EquatorialCoordinates{
			X: equatorial.X,
			Y: equatorial.Y,
			Z: equatorial.Z,
		},
		Heliocentric: heliocentric,

		ArgumentOfPerihelion:        w,
		Eccentricity:                e,
		EccentricAnomaly:            celestial.Radian(E),
		Inclination:                 I,
		LongitudeOfTheAscendingNode: lan,
		LongitudeOfPerihelion:       celestial.Degree(lp).Rads(),
		MeanAnomaly:                 M,
		MeanLongitude:               celestial.Degree(ml).Rads(),
		SemiMajorAxis:               a,
	}
}
