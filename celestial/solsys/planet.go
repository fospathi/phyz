package solsys

// Planet specifies one of the nine possible planets for which a position may be
// calculated.
type Planet int

const (
	// Mercury is the planet Mercury.
	Mercury = Planet(iota)
	// Venus is the planet Venus.
	Venus
	// Earth is the planet Earth.
	Earth
	// Mars is the planet Mars.
	Mars
	// Jupiter is the planet Jupiter.
	Jupiter
	// Saturn is the planet Saturn.
	Saturn
	// Uranus is the planet Uranus.
	Uranus
	// Neptune is the planet Neptune.
	Neptune
	// Pluto is the planet Pluto.
	Pluto
)

// Planets of our Solar System in order of average distance from the Sun.
var Planets []Planet = []Planet{
	Mercury,
	Venus,
	Earth,
	Mars,
	Jupiter,
	Saturn,
	Uranus,
	Neptune,
	Pluto,
}

// PlanetToString is the all-lowercase name of the given planet.
func PlanetToString(planet Planet) string {
	var name string
	switch planet {
	case Mercury:
		name = "mercury"
	case Venus:
		name = "venus"
	case Earth:
		name = "earth"
	case Mars:
		name = "mars"
	case Jupiter:
		name = "jupiter"
	case Saturn:
		name = "saturn"
	case Uranus:
		name = "uranus"
	case Neptune:
		name = "neptune"
	case Pluto:
		name = "pluto"
	}
	return name
}
