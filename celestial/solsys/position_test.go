package solsys_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
)

func TestNewPlanetPosition(t *testing.T) {
	summer1800 := celestial.JD{
		JDN:      celestial.Gregorian{Y: 1800, M: 7, D: 1}.JDN(),
		Fraction: 0.0,
	}
	winter2200 := celestial.JD{
		JDN:      celestial.Gregorian{Y: 2200, M: 12, D: 31}.JDN(),
		Fraction: 0.0,
	}

	/******************************************************************/

	pos := solsys.NewPlanetPosition(solsys.Mercury, summer1800, solsys.Y250)
	aa := celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want := 10
	got := aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = 14
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	pos = solsys.NewPlanetPosition(solsys.Mercury, winter2200, solsys.Y6000)
	aa = celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want = 5
	got = aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = 27
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	/******************************************************************/

	pos = solsys.NewPlanetPosition(solsys.Earth, summer1800, solsys.Y250)
	aa = celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want = 18
	got = aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = -22
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	pos = solsys.NewPlanetPosition(solsys.Earth, winter2200, solsys.Y6000)
	aa = celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want = 6
	got = aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = 23
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	/******************************************************************/

	pos = solsys.NewPlanetPosition(solsys.Jupiter, summer1800, solsys.Y250)
	aa = celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want = 7
	got = aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = 22
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	pos = solsys.NewPlanetPosition(solsys.Jupiter, winter2200, solsys.Y6000)
	aa = celestial.AstronomicalAngle(pos.Equatorial.RightAscension())
	want = 0
	got = aa.HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	aa = celestial.AstronomicalAngle(pos.Equatorial.Declination())
	want = 4
	got = aa.DMSDegrees()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
