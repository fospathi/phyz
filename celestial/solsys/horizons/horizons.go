/*
Package horizons interfaces with the JPL Horizons online solar system data and
ephemeris computation service (https://ssd.jpl.nasa.gov/horizons/).
*/
package horizons

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial"
)

const (
	api        = "https://ssd.jpl.nasa.gov/api/horizons.api"
	apiVersion = "1.2"
)

// DatedPosition of a celestial object.
type DatedPosition struct {
	Date     celestial.JD
	Position maf.Vec
}

// EphemerisSpec specifies the range of dates over which to calculate data for a
// celestial object.
type EphemerisSpec struct {
	Object LookupMatch
	Start  celestial.JD
	Stop   celestial.JD
	Steps  int
}

// Ephemeris of the given celestial object.
//
// Positions are measured in astronomical units (au) and are given relative to
// the Ecliptic reference frame which has its origin at the center of the Sun
// body. The positions are geometric: no corrections or aberrations are applied.
//
// The returned string may be empty. If not it will be the raw response from the
// Horizons API or at least some part of it.
//
// Normally the returned data values are empty zero values if an error is
// returned. However, for the special case of a Horizons API version warning
// then data may still be returned. Since in this case the API version has
// changed however, there is no guarantee that the data is correct.
func Ephemeris(spec EphemerisSpec) ([]DatedPosition, string, error) {
	if spec.Start.Date() > spec.Stop.Date() {
		spec.Start, spec.Stop = spec.Stop, spec.Start
	}
	if spec.Steps < 1 {
		spec.Steps = 1
	}

	base, err := url.Parse(api)
	if err != nil {
		return nil, "", err
	}

	params := url.Values{}
	params.Add("COMMAND", fmt.Sprintf("'%v'", spec.Object.HorizonsName()))
	params.Add("OBJ_DATA", "'NO'")
	// https://ssd-api.jpl.nasa.gov/doc/horizons.html#ephemeris-specific-parameters
	params.Add("EPHEM_TYPE", "'VECTORS'")
	params.Add("VEC_TABLE", "'1'")
	params.Add("START_TIME", fmt.Sprintf("'JD%f'", spec.Start.Date()))
	params.Add("STOP_TIME", fmt.Sprintf("'JD%f'", spec.Stop.Date()))
	params.Add("STEP_SIZE", fmt.Sprintf("'%d'", spec.Steps))
	params.Add("OUT_UNITS", "'AU-D'")
	params.Add("CENTER", "'500@10'") // Sun body center.

	base.RawQuery = params.Encode()

	resp, err := http.Get(base.String())
	if err != nil {
		return nil, "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, "", err
	}

	var (
		apiWarn error
		res     horizonsResult
	)
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, string(body), err
	} else if err = res.checkSingleMatch(); err != nil {
		if !IsAPIVersionWarning(err) {
			return nil, string(body), err
		}
		apiWarn = err
	}

	// SOE => start of ephemeris, EOE => end of ephemeris.
	// Enable s flag with (?s) to allow . to match \n.
	eph := regexp.MustCompile(`(?s)\$\$SOE.+\$\$EOE`)
	match := eph.FindString(res.Result)
	if match == "" {
		return nil, res.Result,
			errors.New("expected an ephemeris: got: " + res.Result)
	}

	var vdp []DatedPosition
	// Use a non greedy everything selector (.+?) after the JDN.
	dp := regexp.MustCompile(
		`(?s)([.[:digit:]]+).+?` +
			`X = ?([+\-.E[:digit:]]+) ` +
			`Y = ?([+\-.E[:digit:]]+) ` +
			`Z = ?([+\-.E[:digit:]]+)`,
	)
	matches := dp.FindAllStringSubmatch(match, -1)

	const groups = 4
	for _, match := range matches {
		// First match is the whole thing, then the capture groups.
		if len(match) != groups+1 {
			return nil, res.Result,
				fmt.Errorf("expected %v regexp groups: %v", groups, match)
		}
		// The capture groups are the JD followed by the X, Y, and Z coords.
		var vf []float64
		for j := 1; j < len(match); j++ {
			f, err := strconv.ParseFloat(match[j], 64)
			if err != nil {
				return nil, res.Result, err
			}
			vf = append(vf, f)
		}
		dp := DatedPosition{
			Date:     celestial.NewJD(vf[0]),
			Position: maf.Vec{X: vf[1], Y: vf[2], Z: vf[3]},
		}
		vdp = append(vdp, dp)
	}

	return vdp, res.Result, apiWarn
}

// Summary of the given celestial object.
func Summary(lm LookupMatch) (string, error) {
	base, err := url.Parse(api)
	if err != nil {
		return "", err
	}

	params := url.Values{}
	params.Add("COMMAND", fmt.Sprintf("'%v'", lm.HorizonsName()))
	params.Add("MAKE_EPHEM", "'NO'")

	base.RawQuery = params.Encode()

	resp, err := http.Get(base.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var res horizonsResult
	if err = json.Unmarshal([]byte(body), &res); err != nil {
		return "", err
	} else if err = res.checkSingleMatch(); err != nil {
		return "", err
	}

	return res.Result, nil
}

type horizonsResult struct {
	Error     string
	Result    string
	Signature signature
}

func (res horizonsResult) checkSingleMatch() error {
	var (
		// An API version error, if any, should be exclusive.
		apiWarn     error
		apiWarnData string
		// If another error occurs with an API version error, still include a
		// warning prefix in case the API change is relevant, but do not wrap
		// the API warning.
		apiWarnPrefix string
	)
	if res.Signature.Version != apiVersion {
		apiWarnData = fmt.Sprintf(
			"expect %v, got %v", apiVersion, res.Signature.Version)
		apiWarnPrefix =
			fmt.Sprintf("(warning: Horizons API version: %v): ", apiWarnData)
		apiWarn = fmt.Errorf("%w: %v", errAPIVersionWarning, apiWarnData)
	}

	if res.Error != "" {
		return fmt.Errorf(apiWarnPrefix+"%w: %v", errHorizonsError, res.Error)
	} else if res.Result == "" ||
		strings.Contains(res.Result, "No matches found") ||
		strings.Contains(res.Result, "Number of matches") {

		return errors.New(apiWarnPrefix + "expect one match: got: " + res.Result)
	}

	return apiWarn
}

// IsAPIVersionWarning reports whether the given error is a warning that the
// Horizons API version has changed but no other error occurred.
func IsAPIVersionWarning(err error) bool {
	return errors.Is(err, errAPIVersionWarning)
}

var errAPIVersionWarning = errors.New("horizons API version error")

// IsHorizonsError reports whether the given error indicates that the error
// field of the Horizons query result is not empty.
func IsHorizonsError(err error) bool {
	return errors.Is(err, errHorizonsError)
}

var errHorizonsError = errors.New("horizons error")
