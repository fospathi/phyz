package horizons_test

import (
	"testing"
	"time"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/phyz/celestial/solsys/horizons"
)

func TestEphemeris(t *testing.T) {

	// If two different data sources give the position of a celestial object,
	// they are said to be in agreement about the position of the object if the
	// distance between them is less than this.
	const tolerance = 0.03 // astronomical units (au).

	// Test asking Horizons for the position of Jupiter at 12 noon (midday) on
	// January 1, 2000.

	matches, err := horizons.Lookup("Jupiter")
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(matches) >= 2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	var jup horizons.LookupMatch
	for _, m := range matches {
		if m.ObjectName == "Jupiter" {
			jup = m
		}
	}

	{
		want := true
		got := jup.ObjectName == "Jupiter"
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	jupEph, _, err := horizons.Ephemeris(horizons.EphemerisSpec{
		Object: jup,
		Start:  celestial.J2000,
		Stop:   celestial.J2000.Add(1),
		Steps:  1,
	})
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(jupEph) > 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Standish refers to Standish E.M., author of "Keplerian Elements for
	// Approximate Positions of the Major Planets", the document upon which the
	// internal ephemeris is based.
	standishJupPos := solsys.NewPlanetPosition(
		solsys.Jupiter, celestial.J2000, solsys.Y250).Ecliptic
	horizonsJupPos := jupEph[0].Position

	{
		want := true
		got := standishJupPos.DistTo(horizonsJupPos) <= tolerance
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Avoid spamming the Horizons API with requests.
	time.Sleep(time.Millisecond * 250)

	// Test asking Horizons for the positions of the Earth-Moon Barycenter at
	// various dates in the time interval 1800 AD to 2050 AD.

	matches, err = horizons.Lookup("Earth-Moon Barycenter")
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(matches) == 1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	emb := matches[0]
	start := celestial.JD{JDN: celestial.Gregorian{Y: 1800, M: 1, D: 1}.JDN()}
	stop := celestial.JD{JDN: celestial.Gregorian{Y: 2050, M: 1, D: 1}.JDN()}
	steps := 20
	embEph, _, err := horizons.Ephemeris(horizons.EphemerisSpec{
		Object: emb,
		Start:  start,
		Stop:   stop,
		Steps:  steps,
	})
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(embEph) == steps+1
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	var (
		vStandishEMBPos = make([]maf.Vec, steps+1)
		vHorizonsEMBPos = make([]maf.Vec, steps+1)
	)

	for i, m := range embEph {
		vHorizonsEMBPos[i] = m.Position
		jd := celestial.NewJD(start.Date() + float64(i)*
			(stop.Date()-start.Date())/float64(steps))
		vStandishEMBPos[i] = solsys.NewPlanetPosition(
			solsys.Earth, jd, solsys.Y250).Ecliptic
	}

	ok := true
	for i := 0; i < steps+1; i++ {
		if vStandishEMBPos[i].DistTo(vHorizonsEMBPos[i]) > tolerance {
			ok = false
			break
		}
	}

	{
		want := true
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test asking Horizons for the positions of the Earth-Moon Barycenter at
	// some invalid dates which are outside the maximum available Horizons
	// ephemeris time span for Earth.
	//
	// https://ssd.jpl.nasa.gov/horizons/time_spans.html

	start = celestial.JD{JDN: celestial.Gregorian{Y: 9990, M: 1, D: 1}.JDN()}
	stop = celestial.JD{JDN: celestial.Gregorian{Y: 10010, M: 1, D: 1}.JDN()}
	steps = 10
	_, _, err = horizons.Ephemeris(horizons.EphemerisSpec{
		Object: emb,
		Start:  start,
		Stop:   stop,
		Steps:  steps,
	})
	if !horizons.IsHorizonsError(err) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "a Horizons error", err)
	}
}
