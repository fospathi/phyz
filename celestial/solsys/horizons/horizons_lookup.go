package horizons

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
)

const (
	lookupAPI        = "https://ssd.jpl.nasa.gov/api/horizons_lookup.api"
	lookupAPIVersion = "1.0"
)

// LookupMatch is one possible match for a lookup.
type LookupMatch struct {
	Aliases            []string `json:"alias"`
	ObjectName         string   `json:"name"`
	PrimaryDesignation string   `json:"pdes"`
	PrimarySPKID       string   `json:"spkid"`
}

// HorizonsName can be given to the Horizons API to produce a unique match.
func (lm LookupMatch) HorizonsName() string {
	if lm.PrimaryDesignation != "" {
		return lm.PrimaryDesignation
	} else if lm.PrimarySPKID != "" {
		return lm.PrimarySPKID
	}
	return lm.ObjectName
}

// Lookup the given name using the Horizons Lookup API.
//
// The search is case insensitive, but space sensitive.
//
// https://ssd-api.jpl.nasa.gov/doc/horizons_lookup.html
func Lookup(name string) ([]LookupMatch, error) {
	if name == "" {
		return nil, nil
	}

	if !allowedLookupCharacters.MatchString(name) {
		return nil,
			errors.New("lookup not allowed: use allowed characters: " + name)
	}

	resp, err := http.Get(lookupAPI + "?sstr=" + url.QueryEscape(name))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res lookupResult
	err = json.Unmarshal(body, &res)
	if err != nil {
		// Try checking for an empty result indicating no matches.
		var emptyLookupResult emptyLookupResult
		if json.Unmarshal([]byte(body), &emptyLookupResult) != nil {
			return nil, err // Return the first error.
		}
		return nil, nil
	}

	if res.Signature.Version != lookupAPIVersion {
		return nil,
			fmt.Errorf("incorrect api version: expected %v, got %v",
				lookupAPIVersion, res.Signature.Version)
	}

	return res.Result, nil
}

type signature struct {
	Source  string
	Version string
}

type lookupResult struct {
	Count     int
	Result    []LookupMatch
	Signature signature
}

// The type of the count field changes from int to a string zero ("0") when
// there are no matches.
type emptyLookupResult struct {
	Count     string
	Signature signature
}

// AllowedLookupCharacters for the Horizons lookup API expressed as a regular
// expression.
//
// Valid lookup names consist of one or more of the following characters in any
// combination:
//
//   - alphanumeric characters;
//
//   - (.) period;
//
//   - ( ) space;
//
//   - (') apostrophe;
//
//   - (/) forward slash.
const AllowedLookupCharacters = `^[[:alnum:] /'\-.]+$`

var allowedLookupCharacters = regexp.MustCompile(AllowedLookupCharacters)
