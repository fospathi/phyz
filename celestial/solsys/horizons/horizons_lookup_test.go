package horizons_test

import (
	"testing"

	"gitlab.com/fospathi/dryad/eqset"
	"gitlab.com/fospathi/phyz/celestial/solsys/horizons"
)

func TestLookup(t *testing.T) {
	// Test a lookup which has a disallowed character in the name.

	_, err := horizons.Lookup("juno #")
	if err == nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "a name error", nil)
	}

	// Test a lookup which has no matching objects.

	matches, err := horizons.Lookup("ZZZZ")
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(matches) == 0
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a lookup for objects with "Juno" in the name.

	matches, err = horizons.Lookup("juno")
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	{
		want := true
		got := len(matches) >= 3
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	names := eqset.New[string]()
	for _, match := range matches {
		names.Add(match.ObjectName)
	}

	{
		want := true
		got := names.ContainsAll(
			"Juno (spacecraft)", "Juno Centaur Stage (spacecraft)", "Juno")
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test a lookup for the aliases of 2004 MN4.

	matches, err = horizons.Lookup("2004 MN4")
	if err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	aliases := eqset.New[string]()
	for _, match := range matches {
		if match.PrimaryDesignation == "2004 MN4" {
			aliases.AddAll(match.Aliases...)
			break
		}
	}

	{
		want := true
		got := aliases.Equals(eqset.New("3264226", "2099942", "K04M04N"))
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
