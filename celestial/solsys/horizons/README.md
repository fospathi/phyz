# Package horizons

```go
import "gitlab.com/fospathi/phyz/celestial/solsys/horizons"
```

Go package horizons generates ephemerides for Solar System objects by interfacing with the JPL [Horizons System](https://ssd.jpl.nasa.gov/horizons/), an online Solar System data and ephemeris computation service.

