package solsys

import (
	"gitlab.com/fospathi/phyz/celestial"
)

// Interval specifies one of the possible time periods over which the orbital
// elements are valid.
type Interval int

const (
	// Y250 specifies a 250 year interval from 1800 AD to 2050 AD.
	Y250 = Interval(iota)
	// Y6000 specifies a 6000 year interval from 3000 BC to 3000 AD.
	Y6000
)

// Start and end years of the orbital element validity intervals in the
// proleptic Gregorian calendar.
const (
	Y250Start = 1800
	Y250End   = 2050

	Y6000Start = -3000
	Y6000End   = 3000
)

const (
	// EclipticObliquity is the angle, measured in degrees, between the ecliptic
	// and equatorial frames at [epoch J2000.0].
	//
	// [epoch J2000.0]:
	// https://en.wikipedia.org/wiki/Epoch_(astronomy)#Julian_dates_and_J2000
	EclipticObliquity = celestial.Degree(23.43928)
)
