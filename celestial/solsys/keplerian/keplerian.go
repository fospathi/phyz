/*
Package keplerian reproduces orbital elements from

	  	Keplerian Elements for Approximate Positions of the Major Planets,
		E M Standish, Solar System Dynamics Group, JPL / Caltech
*/
package keplerian

// Elements1800To2050 is a JSON format string containing six orbital elements
// for each of the planets and is valid for the period 1800 AD to 2050 AD.
const Elements1800To2050 = `{
	"mercury": {
	  "a": {
		"name": "semi-major axis",
		"value": 0.38709927,
		"rate": 3.7e-7
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.20563593,
		"rate": 0.00001906
	  },
	  "I": {
		"name": "inclination",
		"value": 7.00497902,
		"rate": -0.00594749
	  },
	  "L": {
		"name": "mean longitude",
		"value": 252.2503235,
		"rate": 149472.67411175
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 77.45779628,
		"rate": 0.16047689
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 48.33076593,
		"rate": -0.12534081
	  },
	  "extra": null
	},
	"venus": {
	  "a": {
		"name": "semi-major axis",
		"value": 0.72333566,
		"rate": 0.0000039
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.00677672,
		"rate": -0.00004107
	  },
	  "I": {
		"name": "inclination",
		"value": 3.39467605,
		"rate": -0.0007889
	  },
	  "L": {
		"name": "mean longitude",
		"value": 181.9790995,
		"rate": 58517.81538729
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 131.60246718,
		"rate": 0.00268329
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 76.67984255,
		"rate": -0.27769418
	  },
	  "extra": null
	},
	"earth": {
	  "a": {
		"name": "semi-major axis",
		"value": 1.00000261,
		"rate": 0.00000562
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.01671123,
		"rate": -0.00004392
	  },
	  "I": {
		"name": "inclination",
		"value": -0.00001531,
		"rate": -0.01294668
	  },
	  "L": {
		"name": "mean longitude",
		"value": 100.46457166,
		"rate": 35999.37244981
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 102.93768193,
		"rate": 0.32327364
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 0.0,
		"rate": 0.0
	  },
	  "extra": null
	},
	"mars": {
	  "a": {
		"name": "semi-major axis",
		"value": 1.52371034,
		"rate": 0.00001847
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.0933941,
		"rate": 0.00007882
	  },
	  "I": {
		"name": "inclination",
		"value": 1.84969142,
		"rate": -0.00813131
	  },
	  "L": {
		"name": "mean longitude",
		"value": -4.55343205,
		"rate": 19140.30268499
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": -23.94362959,
		"rate": 0.44441088
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 49.55953891,
		"rate": -0.29257343
	  },
	  "extra": null
	},
	"jupiter": {
	  "a": {
		"name": "semi-major axis",
		"value": 5.202887,
		"rate": -0.00011607
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.04838624,
		"rate": -0.00013253
	  },
	  "I": {
		"name": "inclination",
		"value": 1.30439695,
		"rate": -0.00183714
	  },
	  "L": {
		"name": "mean longitude",
		"value": 34.39644051,
		"rate": 3034.74612775
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 14.72847983,
		"rate": 0.21252668
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 100.47390909,
		"rate": 0.20469106
	  },
	  "extra": null
	},
	"saturn": {
	  "a": {
		"name": "semi-major axis",
		"value": 9.53667594,
		"rate": -0.0012506
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.05386179,
		"rate": -0.00050991
	  },
	  "I": {
		"name": "inclination",
		"value": 2.48599187,
		"rate": 0.00193609
	  },
	  "L": {
		"name": "mean longitude",
		"value": 49.95424423,
		"rate": 1222.49362201
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 92.59887831,
		"rate": -0.41897216
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 113.66242448,
		"rate": -0.28867794
	  },
	  "extra": null
	},
	"uranus": {
	  "a": {
		"name": "semi-major axis",
		"value": 19.18916464,
		"rate": -0.00196176
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.04725744,
		"rate": -0.00004397
	  },
	  "I": {
		"name": "inclination",
		"value": 0.77263783,
		"rate": -0.00242939
	  },
	  "L": {
		"name": "mean longitude",
		"value": 313.23810451,
		"rate": 428.48202785
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 170.9542763,
		"rate": 0.40805281
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 74.01692503,
		"rate": 0.04240589
	  },
	  "extra": null
	},
	"neptune": {
	  "a": {
		"name": "semi-major axis",
		"value": 30.06992276,
		"rate": 0.00026291
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.00859048,
		"rate": 0.00005105
	  },
	  "I": {
		"name": "inclination",
		"value": 1.77004347,
		"rate": 0.00035372
	  },
	  "L": {
		"name": "mean longitude",
		"value": -55.12002969,
		"rate": 218.45945325
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 44.96476227,
		"rate": -0.32241464
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 131.78422574,
		"rate": -0.00508664
	  },
	  "extra": null
	},
	"pluto": {
	  "a": {
		"name": "semi-major axis",
		"value": 39.48211675,
		"rate": -0.00031596
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.2488273,
		"rate": 0.0000517
	  },
	  "I": {
		"name": "inclination",
		"value": 17.14001206,
		"rate": 0.00004818
	  },
	  "L": {
		"name": "mean longitude",
		"value": 238.92903833,
		"rate": 145.20780515
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 224.06891629,
		"rate": -0.04062942
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 110.30393684,
		"rate": -0.01183482
	  },
	  "extra": null
	}
  }`

// Elements3000To3000 is a JSON format string containing six orbital elements
// for each of the planets and is valid for the period 3000 BC to 3000 AD.
const Elements3000To3000 = `{
	"mercury": {
	  "a": {
		"name": "semi-major axis",
		"value": 0.38709843,
		"rate": 0
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.20563661,
		"rate": 0.00002123
	  },
	  "I": {
		"name": "inclination",
		"value": 7.00559432,
		"rate": -0.00590158
	  },
	  "L": {
		"name": "mean longitude",
		"value": 252.25166724,
		"rate": 149472.67486623
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 77.45771895,
		"rate": 0.15940013
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 48.33961819,
		"rate": -0.12214182
	  },
	  "extra": null
	},
	"venus": {
	  "a": {
		"name": "semi-major axis",
		"value": 0.72332102,
		"rate": -2.6e-7
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.00676399,
		"rate": -0.00005107
	  },
	  "I": {
		"name": "inclination",
		"value": 3.39777545,
		"rate": 0.00043494
	  },
	  "L": {
		"name": "mean longitude",
		"value": 181.9797085,
		"rate": 58517.8156026
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 131.76755713,
		"rate": 0.05679648
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 76.67261496,
		"rate": -0.27274174
	  },
	  "extra": null
	},
	"earth": {
	  "a": {
		"name": "semi-major axis",
		"value": 1.00000018,
		"rate": -3e-8
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.01673163,
		"rate": -0.00003661
	  },
	  "I": {
		"name": "inclination",
		"value": -0.00054346,
		"rate": -0.01337178
	  },
	  "L": {
		"name": "mean longitude",
		"value": 100.46691572,
		"rate": 35999.37306329
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 102.93005885,
		"rate": 0.3179526
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": -5.11260389,
		"rate": -0.24123856
	  },
	  "extra": null
	},
	"mars": {
	  "a": {
		"name": "semi-major axis",
		"value": 1.52371243,
		"rate": 9.7e-7
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.09336511,
		"rate": 0.00009149
	  },
	  "I": {
		"name": "inclination",
		"value": 1.85181869,
		"rate": -0.00724757
	  },
	  "L": {
		"name": "mean longitude",
		"value": -4.56813164,
		"rate": 19140.29934243
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": -23.91744784,
		"rate": 0.45223625
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 49.71320984,
		"rate": -0.26852431
	  },
	  "extra": null
	},
	"jupiter": {
	  "a": {
		"name": "semi-major axis",
		"value": 5.20248019,
		"rate": -0.00002864
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.0485359,
		"rate": 0.00018026
	  },
	  "I": {
		"name": "inclination",
		"value": 1.29861416,
		"rate": -0.00322699
	  },
	  "L": {
		"name": "mean longitude",
		"value": 34.33479152,
		"rate": 3034.90371757
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 14.27495244,
		"rate": 0.18199196
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 100.29282654,
		"rate": 0.13024619
	  },
	  "extra": {
		"b": -0.00012452,
		"c": 0.0606406,
		"s": -0.35635438,
		"f": 38.35125
	  }
	},
	"saturn": {
	  "a": {
		"name": "semi-major axis",
		"value": 9.54149883,
		"rate": -0.00003065
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.05550825,
		"rate": -0.00032044
	  },
	  "I": {
		"name": "inclination",
		"value": 2.49424102,
		"rate": 0.00451969
	  },
	  "L": {
		"name": "mean longitude",
		"value": 50.07571329,
		"rate": 1222.11494724
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 92.86136063,
		"rate": 0.54179478
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 113.63998702,
		"rate": -0.25015002
	  },
	  "extra": {
		"b": 0.00025899,
		"c": -0.13434469,
		"s": 0.87320147,
		"f": 38.35125
	  }
	},
	"uranus": {
	  "a": {
		"name": "semi-major axis",
		"value": 19.18797948,
		"rate": -0.00020455
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.0468574,
		"rate": -0.0000155
	  },
	  "I": {
		"name": "inclination",
		"value": 0.77298127,
		"rate": -0.00180155
	  },
	  "L": {
		"name": "mean longitude",
		"value": 314.20276625,
		"rate": 428.49512595
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 172.43404441,
		"rate": 0.09266985
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 73.96250215,
		"rate": 0.05739699
	  },
	  "extra": {
		"b": 0.00058331,
		"c": -0.97731848,
		"s": 0.17689245,
		"f": 7.67025
	  }
	},
	"neptune": {
	  "a": {
		"name": "semi-major axis",
		"value": 30.06952752,
		"rate": 0.00006447
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.00895439,
		"rate": 0.00000818
	  },
	  "I": {
		"name": "inclination",
		"value": 1.7700552,
		"rate": 0.000224
	  },
	  "L": {
		"name": "mean longitude",
		"value": 304.22289287,
		"rate": 218.46515314
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 46.68158724,
		"rate": 0.01009938
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 131.78635853,
		"rate": -0.00606302
	  },
	  "extra": {
		"b": -0.00041348,
		"c": 0.68346318,
		"s": -0.10162547,
		"f": 7.67025
	  }
	},
	"pluto": {
	  "a": {
		"name": "semi-major axis",
		"value": 39.48686035,
		"rate": 0.00449751
	  },
	  "e": {
		"name": "eccentricity",
		"value": 0.24885238,
		"rate": 0.00006016
	  },
	  "I": {
		"name": "inclination",
		"value": 17.1410426,
		"rate": 0.00000501
	  },
	  "L": {
		"name": "mean longitude",
		"value": 238.96535011,
		"rate": 145.18042903
	  },
	  "lp": {
		"name": "longitude of perihelion",
		"value": 224.09702598,
		"rate": -0.00968827
	  },
	  "lan": {
		"name": "longitude of the ascending node",
		"value": 110.30167986,
		"rate": -0.00809981
	  },
	  "extra": {
		"b": -0.01262724,
		"c": 0,
		"s": 0,
		"f": 0
	  }
	}
  }`
