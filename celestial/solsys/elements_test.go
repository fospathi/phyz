package solsys_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial/solsys"
	"gitlab.com/fospathi/universal/tezd"

	"gitlab.com/fospathi/maf"
)

func TestNewPlanetsElements(t *testing.T) {
	y250 := solsys.NewPlanetsElements(solsys.Y250)
	y6000 := solsys.NewPlanetsElements(solsys.Y6000)

	{
		want := 0.38709927
		got := y250.Mercury.SemiMajorAxis.Value
		if want != got {
			t.Errorf("\nwant: %v\ngot: %v\n", want, got)
		}
	}

	{
		want := -0.00004107
		got := y250.Venus.Eccentricity.Rate
		if want != got {
			t.Errorf("\nwant: %v\ngot: %v\n", want, got)
		}
	}

	{
		want := "inclination"
		got := y250.Earth.Inclination.Name
		if want != got {
			t.Errorf("\nwant: %v\ngot: %v\n", want, got)
		}
	}

	{
		want := -0.01262724
		got := y6000.Pluto.Extra.B
		if want != got {
			t.Errorf("\nwant: %v\ngot: %v\n", want, got)
		}
	}
}

func TestKeplerRoot(t *testing.T) {
	want := 54.307
	got := maf.RadToDeg(solsys.KeplerRoot(maf.DegToRad(45), 0.2))
	if !tezd.CompareFloat64(want, got, 3) {
		t.Errorf("\nwant: %v\ngot: %v\n", want, got)
	}

	want = -168.86
	got = maf.RadToDeg(solsys.KeplerRoot(maf.DegToRad(-160), 0.8))
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nwant: %v\ngot: %v\n", want, got)
	}
}
