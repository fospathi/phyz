# Package solsys

```go
import "gitlab.com/fospathi/phyz/celestial/solsys"
```

Go package solsys calculates a planet's position from its Keplerian orbital elements. Six orbital elements describe a planet's orbit.

Go package [keplerian](https://gitlab.com/fospathi/phyz/-/tree/master/celestial/solsys/keplerian) contains the actual orbital elements for each planet.

