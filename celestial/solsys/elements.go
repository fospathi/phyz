package solsys

import (
	"encoding/json"
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/phyz/celestial"
	"gitlab.com/fospathi/phyz/celestial/solsys/keplerian"
)

// PlanetsElementsY250 is the orbital elements of the planets of the Solar
// System and they are valid over the time period 1800 AD to 2050 AD.
var PlanetsElementsY250 = NewPlanetsElements(Y250)

// ElementsY250 is a map of nine sets of orbital elements of the planets of the
// Solar System and they are valid over the time period 1800 AD to 2050 AD.
var ElementsY250 = map[Planet]Elements{
	Mercury: PlanetsElementsY250.Mercury,
	Venus:   PlanetsElementsY250.Venus,
	Earth:   PlanetsElementsY250.Earth,
	Mars:    PlanetsElementsY250.Mars,
	Jupiter: PlanetsElementsY250.Jupiter,
	Saturn:  PlanetsElementsY250.Saturn,
	Uranus:  PlanetsElementsY250.Uranus,
	Neptune: PlanetsElementsY250.Neptune,
	Pluto:   PlanetsElementsY250.Pluto,
}

// PlanetsElementsY6000 is the orbital elements of the planets of the Solar
// System and they are valid over the time period 3000 BC to 3000 AD.
var PlanetsElementsY6000 = NewPlanetsElements(Y6000)

// ElementsY6000 is a map of nine sets of orbital elements of the planets of the
// Solar System and they are valid over the time period 3000 BC to 3000 AD.
var ElementsY6000 = map[Planet]Elements{
	Mercury: PlanetsElementsY6000.Mercury,
	Venus:   PlanetsElementsY6000.Venus,
	Earth:   PlanetsElementsY6000.Earth,
	Mars:    PlanetsElementsY6000.Mars,
	Jupiter: PlanetsElementsY6000.Jupiter,
	Saturn:  PlanetsElementsY6000.Saturn,
	Uranus:  PlanetsElementsY6000.Uranus,
	Neptune: PlanetsElementsY6000.Neptune,
	Pluto:   PlanetsElementsY6000.Pluto,
}

// NewPlanetsElements creates and initialises a new planets elements which is
// valid over the argument time period.
func NewPlanetsElements(period Interval) PlanetsElements {
	pe := PlanetsElements{}
	if period == Y250 {
		json.Unmarshal([]byte(keplerian.Elements1800To2050), &pe)
	} else {
		json.Unmarshal([]byte(keplerian.Elements3000To3000), &pe)
	}
	return pe
}

// PlanetsElements is a collection of nine sets of orbital elements for the
// planets of the Solar System.
type PlanetsElements struct {
	Mercury Elements
	Venus   Elements
	Earth   Elements
	Mars    Elements
	Jupiter Elements
	Saturn  Elements
	Uranus  Elements
	Neptune Elements
	Pluto   Elements
}

// Elements is a collection of six orbital elements for a planet plus some extra
// arbitrary constants.
type Elements struct {
	// The semi-major axis, with respect to the mean equinox and ecliptic of
	// J2000.0, measured in astronomical units (au).
	//
	// Its rate is measured in astronomical units/century (au/cty).
	SemiMajorAxis Element `json:"a"`
	// The eccentricity, with respect to the mean equinox and ecliptic of J2000.0.
	//
	// Its rate is measured in per century (/cty).
	Eccentricity Element `json:"e"`
	// The inclination, with respect to the mean equinox and ecliptic of
	// J2000.0, measured in degrees (deg).
	//
	// Its rate is measured in degrees/century (deg/cty).
	Inclination Element `json:"I"`
	// The mean longitude, with respect to the mean equinox and ecliptic of
	// J2000.0, measured in degrees (deg).
	//
	// Its rate is measured in degrees/century (deg/cty).
	MeanLongitude Element `json:"L"`
	// The longitude of perihelion, with respect to the mean equinox and
	// ecliptic of J2000.0, measured in degrees (deg).
	//
	// Its rate is measured in degrees/century (deg/cty).
	LongitudeOfPerihelion Element `json:"lp"`
	// The longitude of the ascending node, with respect to the mean equinox and
	// ecliptic of J2000.0, measured in degrees (deg).
	//
	// Its rate is measured in degrees/century (deg/cty).
	LongitudeOfTheAscendingNode Element `json:"lan"`
	// Note that these extra constants may not always be present after
	// un-marshalling the raw data.
	Extra Extra
}

// Element is an orbital element.
type Element struct {
	// The name of the orbital element.
	Name string
	// The base value of the orbital element.
	Value float64
	// The rate of the orbital element which is used to improve the accuracy of
	// the base value.
	Rate float64
}

// OnDate returns the computed value of the receiver element on the argument
// date.
func (e Element) OnDate(jd celestial.JD) float64 {
	return e.Value + jd.T()*e.Rate
}

// Extra contains some numerical constants which may be used in the calculations
// of the planet positions.
//
// Note that these constants are only needed and provided for a few planets
// within the longer time period.
type Extra struct {
	B, C, S, F float64
}

// MeanAnomalyExtra returns an extra accuracy improving component which shall in
// a few cases be added to the mean anomaly when calculating a planet's
// position.
//
// The returned component is non zero only when
//   - the time period is 3000 BC to 3000 AD
//   - the planet is one of: Jupiter, Saturn, Uranus, Neptune, Pluto
func (e Extra) MeanAnomalyExtra(
	planet Planet,
	period Interval,
	jd celestial.JD,
) float64 {

	mae := 0.0
	if Y6000 == period {
		switch planet {
		case Jupiter, Saturn, Uranus, Neptune, Pluto:
			T := jd.T()
			mae = e.B*T*T +
				e.C*celestial.Degree(e.F*T).Cos() +
				e.S*celestial.Degree(e.F*T).Sin()
		}
	}
	return mae
}

// KeplerRoot numerically approximates the eccentric anomaly (E), in radians,
// using Kepler's equation: M = E - e sin(E)
//
// The meanAnomaly (M) should be given in radians in the range -PI..PI
// inclusive. e is the eccentricity which is a number between 0 and 1.
func KeplerRoot(M, e float64) float64 {
	const tolerance float64 = 1e-6
	const maxIterations int = 25
	// Kepler's equation, rearranged so that all non zero terms are on one side.
	// M = E - e sin(E)
	// 0 = E - e sin(E) - M
	f := func(E float64) float64 {
		return E - e*math.Sin(E) - M
	}
	d := func(E float64) float64 {
		return 1 - e*math.Cos(E)
	}

	// When numerically solving Kepler's equation the mean anomaly (M) makes a
	// good initial guess.
	return maf.NewtonsMethod(f, d, M, tolerance, maxIterations)
}
