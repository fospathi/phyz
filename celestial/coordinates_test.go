package celestial_test

import (
	"testing"

	"gitlab.com/fospathi/phyz/celestial"
)

func TestAstronomicalAngle_HMSHours(t *testing.T) {
	want := 0
	got := celestial.AstronomicalAngle(10).HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 6
	got = celestial.AstronomicalAngle(90).HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 18
	got = celestial.AstronomicalAngle(-90).HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 18
	got = celestial.AstronomicalAngle(-450).HMSHours()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestEquatorialCoordinates_Declination(t *testing.T) {
	want := 90.0
	got := float64(celestial.EquatorialCoordinates{X: 0, Y: 0, Z: 1}.Declination())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -90.0
	got = float64(celestial.EquatorialCoordinates{X: 0, Y: 0, Z: -1}.Declination())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 0.0
	got = float64(celestial.EquatorialCoordinates{X: 1, Y: 1, Z: 0}.Declination())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 45.0
	got = float64(celestial.EquatorialCoordinates{X: 1, Y: 0, Z: 1}.Declination())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -45.0
	got = float64(celestial.EquatorialCoordinates{X: 1, Y: 0, Z: -1}.Declination())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestEquatorialCoordinates_RightAscension(t *testing.T) {
	want := 0.0
	got := float64(celestial.EquatorialCoordinates{X: 0, Y: 0, Z: 1}.RightAscension())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = float64(celestial.EquatorialCoordinates{X: 0, Y: 0, Z: -1}.RightAscension())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 90.0
	got = float64(celestial.EquatorialCoordinates{X: 0, Y: 1, Z: 1}.RightAscension())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -90.0
	got = float64(celestial.EquatorialCoordinates{X: 0, Y: -1, Z: 1}.RightAscension())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
