package celestial

import (
	"math"
	"time"
)

// J2000 is Julian Date 2451545.0, that is Gregorian date 2000 Jan 1 at 12h
// (noon).
//
// https://en.wikipedia.org/wiki/Epoch_(astronomy)#Julian_dates_and_J2000
var J2000 = JD{JDN: JDN(2451545), Fraction: 0}

// NewJD from the given Julian Date.
//
// The integer component of the given Julian Date is the Julian Day Number and
// the fractional component is the fraction of a solar day, in the range 0..1,
// since the preceding noon.
func NewJD(date float64) JD {
	jdn, frac := math.Modf(date)
	return JD{JDN: JDN(jdn), Fraction: frac}
}

// Now is the current time as a Julian Date.
func Now() JD {
	t := time.Now().UTC()
	var dayStart time.Time
	if t.Hour() < 12 {
		t2 := t.Add(-time.Hour * 12)
		dayStart = time.Date(t2.Year(), t2.Month(), t2.Day(), 12,
			0, 0, 0, time.UTC)
	} else {
		dayStart = time.Date(t.Year(), t.Month(), t.Day(), 12,
			0, 0, 0, time.UTC)
	}
	return JD{
		Fraction: float64(t.Sub(dayStart)) / float64(time.Hour*24),
		JDN: Gregorian{
			Y: GregorianYear(dayStart.Year()),
			M: int(dayStart.Month()),
			D: dayStart.Day(),
		}.JDN(),
	}
}

// JD is a Julian Date: https://en.wikipedia.org/wiki/Julian_day.
type JD struct {
	JDN JDN
	// The fraction of a solar day, in the range 0..1, since the preceding noon.
	Fraction float64
}

// Add the given duration measured in units of Julian days to the receiver date
// and return the resulting date.
func (jd JD) Add(duration float64) JD {
	date := jd.Date() + duration
	day := math.Floor(date)
	return JD{JDN: JDN(day), Fraction: date - day}
}

// Date returns the sum of the receiver's Julian Day Number and fraction.
func (jd JD) Date() float64 {
	return float64(jd.JDN) + jd.Fraction
}

// T is the number of Julian centuries from the receiver date to the J2000
// epoch.
func (jd JD) T() float64 {
	return (jd.Date() - J2000.Date()) / (JulianMeanYear * 100)
}

// A JDN is a specific Julian Day Number:
// https://en.wikipedia.org/wiki/Julian_day.
//
// The Julian Day Number 0 corresponds to the calendar day starting at noon on:
//
//   - JulianDay{Y:-4712, M:1, N:1}, that is, Julian 4713 BC January 1st;
//
//   - GregorianDay{Y:-4713, M:11, D:24}, that is, Gregorian 4714 BC November
//     24th.
type JDN int

// Gregorian returns the Gregorian calendar day on which the solar day with the
// receiver Julian Day Number begins.
func (jdn JDN) Gregorian() Gregorian {
	const g0 int = 1721120 // Gregorian(0, 3, 1) <==> Julian Day Number 1721120
	shift := 0
	if int(jdn) < g0 {
		shift = int(math.Ceil(float64(g0-int(jdn))/GregorianRep)) * GregorianRep
	}
	dG0 := int(jdn) + shift - g0
	yG0 := int(float64(dG0) / GregorianMeanYear)
	daysSinceMarch1st := dG0 - (yG0 * 365) - (yG0 / 4) + (yG0 / 100) - (yG0 / 400)
	yJDN := yG0 - int(float64(shift)/GregorianMeanYear)
	y, m, d := YMD(daysSinceMarch1st, GregorianYear(yJDN+1).IsLeap())
	return Gregorian{Y: GregorianYear(yJDN + y), M: m, D: d}
}

// Julian returns the Julian calendar day on which the solar day with the
// receiver Julian Day Number begins.
func (jdn JDN) Julian() Julian {
	const j0 int = 1721118 // Julian(0, 3, 1) <==> Julian Day Number 1721118
	shift := 0
	if int(jdn) < j0 {
		shift = int(math.Ceil(float64(j0-int(jdn))/JulianRep)) * JulianRep
	}
	dJ0 := int(jdn) + shift - j0
	yJ0 := int(float64(dJ0) / JulianMeanYear)
	daysSinceMarch1st := dJ0 - (yJ0 * 365) - (yJ0 / 4)
	yJDN := yJ0 - int(float64(shift)/JulianMeanYear)
	y, m, d := YMD(daysSinceMarch1st, JulianYear(yJDN+1).IsLeap())
	return Julian{Y: JulianYear(yJDN + y), M: m, D: d}
}

// YMD returns, for the date which is the argument number of days since calendar
// day {Y:0, M: 3, D:1}, its year (can be either 0 or 1), month (1..12), and day
// (1..31).
//
// The argument number of days should not exceed 365 by more than a few days.
//
// The argument bool value shall be true if year 1 is a leap year.
func YMD(days int, precedesLeap bool) (y int, m int, d int) {
	var monthDays [12]int
	if precedesLeap {
		monthDays = marchCalLeap
	} else {
		monthDays = marchCalNonLeap
	}
	for ; m < 12; m++ {
		if days < monthDays[m] {
			break
		}
		days -= monthDays[m]
	}
	d = days + 1
	m += 3
	if m > 12 {
		m -= 12
		y++
	}
	return
}
