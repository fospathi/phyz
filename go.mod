module gitlab.com/fospathi/phyz

go 1.20

require (
	gitlab.com/fospathi/dryad v0.0.0-20230416143847-ed420eedd34d
	gitlab.com/fospathi/maf v0.0.0-20230417003027-12724b51e9e7
	gitlab.com/fospathi/universal v0.0.0-20230417003025-64a421a94730
	gitlab.com/fospathi/wire v0.0.0-20230417003033-d87f22b3963f
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29 // indirect
)

replace gitlab.com/fospathi/dryad => ../dryad

replace gitlab.com/fospathi/maf => ../maf

replace gitlab.com/fospathi/mechane => ../mechane

replace gitlab.com/fospathi/universal => ../universal

replace gitlab.com/fospathi/wire => ../wire
